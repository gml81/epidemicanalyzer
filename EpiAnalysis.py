import copy
from multiprocessing import Process
from sim.EpidemicSimulator import EpideSim
import numpy as np
import matplotlib.pyplot as plt
from random import gauss
from sim.EpiAgeDistribution import generateAgeDistribution

#####################################################################################
# Baseline simulation, i.e. no response or action was enforced by any
# authorities
#####################################################################################
def runBaselineSimulation(params):

    params_adj = copy.deepcopy(params)

    sim = EpideSim(params_adj, 'Baseline')
    sim.runSimulation()

#####################################################################################
# No self-isolation
#####################################################################################
def runNoSelfIsolation(params):

    params_adj = copy.deepcopy(params)

    params_adj['selfIsolation'] = False

    sim = EpideSim(params_adj, 'No Self-Isolation')
    sim.runSimulation()

#####################################################################################
# Herd Immunity Test 01
#####################################################################################
def runHerdImmunity01(params):

    params_adj = copy.deepcopy(params)

    params_adj['contactRate'] = 4
    
    sim = EpideSim(params_adj, 'Contact Rate = ' + str(params_adj['contactRate']))
    sim.runSimulation()

#####################################################################################
# Herd Immunity Test 02
#####################################################################################
def runHerdImmunity02(params):

    params_adj = copy.deepcopy(params)

    params_adj['contactRate'] = 8
    
    sim = EpideSim(params_adj, 'Contact Rate = ' + str(params_adj['contactRate']))
    sim.runSimulation()

#####################################################################################
# Shortening of disease
#####################################################################################
def runRaoultProtocol(params):

    params_adj = copy.deepcopy(params)

    params_adj['daysToRecoverWithTreatment'] = 10
    
    sim = EpideSim(params_adj, 'Shortened Recovery Period')
    sim.runSimulation()

#####################################################################################
# Baseline simulation plus cyclical lockdown
#####################################################################################
def runWithCyclicalLockdown(params):
    params_adj = copy.deepcopy(params)

    params_adj['lockdownActive'] = True
    params_adj['activateLockdownCases'] = 150
    params_adj['clusterSocialDistanceType'] = 'Medium'
    params_adj['liftLockdownCases'] = 150

    sim = EpideSim(params_adj, 'Suppression Approach')
    sim.runSimulation()

#####################################################################################
# Baseline simulation plus one lockdown
#####################################################################################
def runWithOneLockdown(params):
    params_adj = copy.deepcopy(params)

    params_adj['lockdownActive'] = True
    params_adj['clusterSocialDistanceType'] = 'Medium'
    params_adj['activateLockdownCases'] = 150
    params_adj['liftLockdownCases'] = -1

    sim = EpideSim(params_adj, 'Eradication Approach')
    sim.runSimulation()

#####################################################################################
# More ICU
#####################################################################################
def runMoreICU(params):
    params_adj = copy.deepcopy(params)

    params_adj['MaxHospitalCapacity'] = 9999

    sim = EpideSim(params_adj, 'Maximum ICU Capacity')
    sim.runSimulation()

#####################################################################################
# Medium social distancing
#####################################################################################
def runMediumSocialDistancing(params):
    params_adj = copy.deepcopy(params)

    params_adj['clusterSocialDistanceType'] = 'Medium'

    sim = EpideSim(params_adj, 'Medium Social Distancing')
    sim.runSimulation()

#####################################################################################
# Global variables
#####################################################################################

nbrOfClusters = 100
clusterInteraction = 0.2
clusterInteractionVar = 0.1
averageAge = 35

""" 
    ********************************************************************************
    Epidemic analysis
    ********************************************************************************
""" 
results = dict()

params = {
    'populationSize' : 100,
    'epochs' : 1000,
    'numberOfClusters' : nbrOfClusters,
    'clusterSocialDistanceType' : 'Low',
    'interClusterSocialDistanceType' : 'Low',
    'contactRate' : 2.3,
    'daysToRecover' : 20,
    'daysToRecoverWithTreatment' : -1,
    'selfIsolation' : True,
    'incubationPeriod' : 5,
    'percAsymptomatic' : 50,
    'lockdownActive' : False,
    'liftLockdownCases' : 20,
    'activateLockdownCases' : 50,
    'interactions' : None,
    'interactionsWeights' : None,
    'childrenImmune' : True,
    'MaxHospitalCapacity' : 24,
    'daysToICUAdmission' : 12
}

#####################################################################################
# Generate Interactions
#####################################################################################
def generateInteractions():
    # Cluster interactions
    # ====================
    interactions = np.random.choice([0, 1], 
        size=(nbrOfClusters, nbrOfClusters), p=[4.7/5, 0.3/5])

    # Create interaction weights
    interactionsWeights = np.random.normal(clusterInteraction, clusterInteractionVar, \
        size=[nbrOfClusters, nbrOfClusters])

    # Update weights where negative
    interactionsWeights = np.where(interactionsWeights < 0, clusterInteractionVar / 1.0, \
        interactionsWeights) 

    params['interactions'] = interactions
    params['interactionsWeights'] = interactionsWeights

    # All clusters self interact
    np.fill_diagonal(interactions, 1)

    plt.matshow(interactionsWeights)
    plt.colorbar()
    plt.matshow(interactions)
    plt.colorbar()
    plt.show()

#####################################################################################
# Main function and scenarios
#####################################################################################
if __name__ == '__main__':

    # Generate interactions for all processes
    generateInteractions()
    
    # Generate age distribution
    params['ageDist'] = generateAgeDistribution(averageAge)

    p9 = Process(target=runMoreICU, args=(params,))
    p9.start()

    p = Process(target=runBaselineSimulation, args=(params,))
    p.start()

    p2 = Process(target=runWithCyclicalLockdown, args=(params,))
    p2.start()
    
    p3 = Process(target=runWithOneLockdown, args=(params,))
    p3.start()

    p4 = Process(target=runNoSelfIsolation, args=(params,))
    p4.start()
    
    p5 = Process(target=runMediumSocialDistancing, args=(params,))
    p5.start()

    p6 = Process(target=runHerdImmunity01, args=(params,))
    p6.start()

    p7 = Process(target=runHerdImmunity02, args=(params,))
    p7.start()

    p8 = Process(target=runRaoultProtocol, args=(params,))
    p8.start()

    p2.join()
    p3.join()
    p4.join()
    p5.join()
    p6.join()
    p7.join()
    p8.join()
    p.join()
    p9.join()