import copy
import matplotlib.pyplot as plt
import numpy as np
from model.EpiSEIR import EpiSEIR
from model.EpiSIRCovid import EpiSIRCovid

from multiprocessing import Process

""" 
    ********************************************************************************
    Epidemic Analysis using SIR-COVID Model
    ********************************************************************************
"""
nbrOfClusters = 10
clusterInteraction = 0.2
clusterInteractionVar = 0.1

params = {
    'r' : 2.3,                       # Contact rate
    'd' : 0.00,                      # Death rate
    's' : 0.4,                       # Self-isolation rate
    'h' : 0.06,                      # Hospitalization rate
    'dh' : 0.5,                      # Death rate in hospital
    'tauE' : 5,                      # Incubation period
    'tauR' : 20,                     # Recovery period
    'tauD' : 15,                     # Time to die
    'tauS' : 2,                      # Onset of symptoms
    'tauH' : 12,                     # Hospital admission
    'tauDH' : 8,                     # Hospital discharge on death
    'tauRH' : 8,                     # Hospital discharge on recovery
    'maxEpochs' : 2000,
    'E0' : 1,                        # Initially Exposed
    'S0' : 600,                      # Initially Susceptible
    'M0' : 400,                      # Initially Immune
    'H_max' : 24,                    # Max number of ICU beds per 100,000
    'growthFunction' : 'Exp',        # Linear, Gompertz, Constant, Exp
    'liftLockdownActiveCases' : -1,  # Active cases when lockdown is lifted
    'lockdownActiveCases' : -1,      # Active cases before lockdown is activated
    'nbrOfClusters' : nbrOfClusters  # Number of clusters
}

# Generate Interactions
# =====================
def generateInteractions():
    # Cluster interactions
    interactions = np.random.choice([0, 1], 
        size=(nbrOfClusters, nbrOfClusters), p=[3.7/5, 1.3/5])

    # Create interaction weights
    interactionsWeights = np.random.normal(clusterInteraction, clusterInteractionVar, \
        size=[nbrOfClusters, nbrOfClusters])

    # Update weights where negative
    interactionsWeights = np.where(interactionsWeights < 0, clusterInteractionVar / 1.0, \
        interactionsWeights) 

    params['interactions'] = interactions
    params['interactionsWeights'] = interactionsWeights

    # All clusters self interact
    np.fill_diagonal(interactions, 1)

    plt.matshow(interactionsWeights)
    plt.colorbar()
    plt.savefig('./data_model/SEIR - weights.png')

    plt.matshow(interactions)
    plt.colorbar()

    plt.savefig('./data_model/SEIR - interactions.png')
    plt.show()

# ===================================================
# Baseline
# ===================================================
def runSIRCOVID(params):
    params_adj = copy.deepcopy(params)

    sim = EpiSIRCovid(params_adj, "SIR-COVID - Baseline")
    sim.runSimulation()

# ===================================================
# Baseline - SEIR
# ===================================================
def runSEIRCOVID(params):
    params_adj = copy.deepcopy(params)

    sim100 = EpiSEIR(params_adj, "SEIR-COVID - Baseline")
    sim100.runSimulation()

# ===================================================
# With No Self-Isolation
# ===================================================
def runSIRCovidNoSelfIsolation(params):
    params_adj = copy.deepcopy(params)

    params_adj['s'] = 0

    sim2 = EpiSIRCovid(params_adj, "SIR-COVID - No Self-Isolation")
    sim2.runSimulation()

# ===================================================
# With No Self-Isolation - SEIR
# ===================================================
def runSEIRCovidNoSelfIsolation(params):
    params_adj = copy.deepcopy(params)

    params_adj['s'] = 0

    sim200 = EpiSEIR(params_adj, "SEIR-COVID - No Self-Isolation")
    sim200.runSimulation()

# ===================================================
# Limited Hospital Beds
# ===================================================
def runSIRCovidUnLimitedIcuBeds(params):
    params_adj = copy.deepcopy(params)

    params_adj['H_max'] = 9999

    sim3 = EpiSIRCovid(params_adj, "SIR-COVID - Unlimited ICU = " + str(params_adj['H_max']))
    sim3.runSimulation()

# ===================================================
# Limited Hospital Beds - SEIR
# ===================================================
def runSEIRCovidUnLimitedIcuBeds(params):
    params_adj = copy.deepcopy(params)

    params_adj['H_max'] = 9999

    sim300 = EpiSEIR(params_adj, "SEIR-COVID - Unlimited ICU = " + str(params_adj['H_max']))
    sim300.runSimulation()

# ===================================================
# Larger S0
# ===================================================
def runSIRCovidLargerS0(params):
    params_adj = copy.deepcopy(params)

    params_adj['S0'] = 5000
    params_adj['M0'] = 5000

    sim4 = EpiSIRCovid(params_adj, "SIR-COVID - Immune 50%")
    sim4.runSimulation()

# ===================================================
# Larger S0 - SEIR
# ===================================================
def runSEIRCovidLargerS0(params):
    params_adj = copy.deepcopy(params)

    params_adj['S0'] = 5000
    params_adj['M0'] = 5000

    sim400 = EpiSEIR(params_adj, "SEIR-COVID - Immune 50%")
    sim400.runSimulation()

# ===================================================
# Apply social distancing
# ===================================================
def runSIRCovidLockDown(params):
    params_adj = copy.deepcopy(params)

    params_adj['lockdownActiveCases'] = 100

    sim5 = EpiSIRCovid(params_adj, "SIR-COVID - Lockdown")
    sim5.runSimulation()

# ===================================================
# Apply social distancing - SEIR
# ===================================================
def runSEIRCovidLockDown(params):
    params_adj = copy.deepcopy(params)

    params_adj['lockdownActiveCases'] = 100

    sim500 = EpiSEIR(params_adj, "SEIR-COVID - Lockdown")
    sim500.runSimulation()

# ===================================================
# Apply social distancing - SEIR - Clusters
# ===================================================
def runSEIRLockdownClusters(params):
    params_adj = copy.deepcopy(params)

    params_adj['lockdownActiveCases'] = 50

    sim500 = EpiSEIR(params_adj, "SEIR-COVID Clusters - Lockdown")
    sim500.runSimulation()

# ===================================================
# Apply Shorter Recovery
# ===================================================
def runSIRCovidShorterRecovery(params):
    params_adj = copy.deepcopy(params)

    params_adj['tauR'] = 10

    sim6 = EpiSIRCovid(params_adj, "SIR-COVID - Shorter Recovery = " + str(params_adj['tauR']))

    sim6.runSimulation()

# ===================================================
# Apply Shorter Recovery - SEIR
# ===================================================
def runSEIRCovidShorterRecovery(params):
    params_adj = copy.deepcopy(params)

    params_adj['tauR'] = 10

    sim6 = EpiSEIR(params_adj, "SEIR-COVID - Shorter Recovery = " + str(params_adj['tauR']))

    sim6.runSimulation()

# ===================================================
# Apply Cyclical Lockdown
# ===================================================
def runSIRCovidCyclicalLockdown(params):
    params_adj = copy.deepcopy(params)

    params_adj['lockdownActiveCases'] = 150
    params_adj['liftLockdownActiveCases'] = 150

    sim7 = EpiSIRCovid(params_adj, "SIR-COVID - Cyclical Lockdown")

    sim7.runSimulation()

# ===================================================
# Apply Cyclical Lockdown - SEIR
# ===================================================
def runSEIRCovidCyclicalLockdown(params):
    params_adj = copy.deepcopy(params)

    params_adj['lockdownActiveCases'] = 150
    params_adj['liftLockdownActiveCases'] = 150

    sim700 = EpiSEIR(params_adj, "SEIR-COVID - Cyclical Lockdown")
    sim700.runSimulation()

# ===================================================
# Apply Cyclical Lockdown - SEIR - Clusters
# ===================================================
def runSEIRCyclicalLockdownClusters(params):
    params_adj = copy.deepcopy(params)

    params_adj['lockdownActiveCases'] = 50
    params_adj['liftLockdownActiveCases'] = 50

    sim710 = EpiSEIR(params_adj, "SEIR-COVID Clusters - Cyclical Lockdown")
    sim710.runSimulation()

# ===================================================
# Higher Contact Rate
# ===================================================
def runSIRCovidHigherContactRate(params):
    params_adj = copy.deepcopy(params)

    params_adj['r'] = 10

    sim8 = EpiSIRCovid(params_adj, "SIR-COVID - Higher Contact Rate = " + str(params_adj['r']))
    sim8.runSimulation()

# ===================================================
# Higher Contact Rate - SEIR
# ===================================================
def runSEIRCovidHigherContactRate(params):
    params_adj = copy.deepcopy(params)

    params_adj['r'] = 10

    sim800 = EpiSEIR(params_adj, "SEIR-COVID - Higher Contact Rate = " + str(params_adj['r']))
    sim800.runSimulation()

# ===================================================
# Unmitigated
# ===================================================
def runSIRCovidUnmitigated(params):
    params_adj = copy.deepcopy(params)

    params_adj['s'] = 0
    params_adj['h'] = 0
    params_adj['H_max'] = 9999
    params_adj['lockdownActiveCases'] = -1
    params_adj['liftLockdownActiveCases'] = -1

    sim9 = EpiSIRCovid(params_adj, "SIR-COVID - Unmitigated Setting")
    sim9.runSimulation()

# ===================================================
# Unmitigated - SEIR
# ===================================================
def runSEIRCovidUnmitigated(params):
    params_adj = copy.deepcopy(params)

    params_adj['s'] = 0
    params_adj['h'] = 0
    params_adj['H_max'] = 9999
    params_adj['lockdownActiveCases'] = -1
    params_adj['liftLockdownActiveCases'] = -1

    sim900 = EpiSEIR(params_adj, "SEIR-COVID - Unmitigated Setting")
    sim900.runSimulation()

#####################################################################################
# Main function and scenarios
#####################################################################################
if __name__ == '__main__':
    
    generateInteractions()

    """
    p1 = Process(target=runSIRCOVID, args=(params,))
    p1.start()

    p2 = Process(target=runSIRCovidLargerS0, args=(params,))
    p2.start()
    
    p3 = Process(target=runSIRCovidUnLimitedIcuBeds, args=(params,))
    p3.start()

    p4 = Process(target=runSIRCovidNoSelfIsolation, args=(params,))
    p4.start()

    p5 = Process(target=runSIRCovidLockDown, args=(params,))
    p5.start()

    p6 = Process(target=runSIRCovidShorterRecovery, args=(params,))
    p6.start()

    p7 = Process(target=runSIRCovidCyclicalLockdown, args=(params,))
    p7.start()

    p8 = Process(target=runSIRCovidHigherContactRate, args=(params,))
    p8.start()

    p9 = Process(target=runSIRCovidUnmitigated, args=(params,))
    p9.start()
    """
    p100 = Process(target=runSEIRCOVID, args=(params,))
    p100.start()

    p910 = Process(target=runSEIRLockdownClusters, args=(params,))
    p910.start()

    p920 = Process(target=runSEIRCyclicalLockdownClusters, args=(params,))
    p920.start()

    """
    p200 = Process(target=runSEIRCovidLargerS0, args=(params,))
    p200.start()

    p300 = Process(target=runSEIRCovidUnLimitedIcuBeds, args=(params,))
    p300.start()

    p400 = Process(target=runSEIRCovidNoSelfIsolation, args=(params,))
    p400.start()

    p500 = Process(target=runSEIRCovidLockDown, args=(params,))
    p500.start()

    p600 = Process(target=runSEIRCovidShorterRecovery, args=(params,))
    p600.start()

    p700 = Process(target=runSEIRCovidCyclicalLockdown, args=(params,))
    p700.start()

    p800 = Process(target=runSEIRCovidHigherContactRate, args=(params,))
    p800.start()

    p900 = Process(target=runSEIRCovidUnmitigated, args=(params,))
    p900.start()

    p1.join()
    p2.join()
    p3.join()
    p4.join()
    p5.join()
    p6.join()
    p7.join()
    p8.join()
    p9.join()
    
    p200.join()
    p300.join()
    p400.join()
    p500.join()
    p600.join()
    p700.join()
    p800.join()
    p900.join()
    """

    p100.join()
    p910.join()
    p920.join()