import numpy as np
import matplotlib.pyplot as plt
import csv

""" 
    ********************************************************************************
    Epidemic Results
    ********************************************************************************
"""

class EpiResults:

     # Constructor
    # ===========
    def __init__(self, tag):

        # Parameters
        self._tag = tag

    # Plot results
    # ============
    def plotResults(self, I, S, E, M, L, D, H, LK):
        
        t = range(len(I))

        fig, ax = plt.subplots(3, 2)
        fig.suptitle(self._tag, fontsize=16)
        fig.set_size_inches(11, 8)
        fig.tight_layout()

        totInfections = []
        for k in t:
            totInfections.append(I[k] + H[k] + L[k] + E[k])

        # Day on day changes
        DOD = []
        for k in range(len(I) - 1):
            DOD.append(totInfections[k + 1] / totInfections[k])

        ax[0, 0].plot(t, totInfections, 'r', label='Infected')
        ax[0, 0].plot(t, I, 'y', label='Infectious')
        ax[0, 0].plot(t, L, 'g', label='Self Isolating')
        ax[0, 0].plot(t, E, 'c', label='Incubating')

        ax[0, 1].plot(t, M, 'b', label='Immune')
        ax[0, 1].plot(t, S, 'm', label='Susceptible')

        ax[1, 0].plot(t, D, 'r', label='Cumulative Death Rate')
        ax[1, 0].plot(t, LK, 'k', label='Lockdown')

        ax[1, 1].plot(t, H, 'k', label='Hospitalized')
        ax[2, 1].plot(t[1:], DOD, 'b', label='Day on day')

        # Annotations
        annotations1 = list(map(int, totInfections))
        annotations2 = list(map(int, M))
        annotations4 = list(map(int, S))
        annotations5 = list(map(int, D))

        # Annotation steps, minimum 1
        step = max(int(len(I) / 10.0), 1)

        for i, txt in enumerate(annotations1):
            if i % step == 0:
                ax[0, 0].annotate(txt, (t[i], totInfections[i]))

        for i, txt in enumerate(annotations2):
            if i % step == 0:
                ax[0, 1].annotate(txt, (t[i], M[i]))

        for i, txt in enumerate(annotations4):
            if i % step == 0:
                ax[0, 1].annotate(txt, (t[i], S[i]))

        """
        for i, txt in enumerate(annotations3):
            if i % step == 0:
                ax[1, 0].annotate(txt, (t[i], self._infectionClustersData[i]))
        """

        for i, txt in enumerate(annotations5):
            if i % step == 0:
                ax[1, 0].annotate(txt, (t[i], D[i]))

        ax[0, 0].legend()
        ax[0, 1].legend()
        ax[1, 0].legend()
        ax[1, 1].legend()
        ax[2, 1].legend()

        plt.subplots_adjust(top=0.9)
        plt.savefig('./data_model/' + self._tag + '.png')
        plt.show()

    # Plot results
    # ============
    def plotResults2(self, I, S, E, M, L, H, D, LK, IC):
        
        t = range(len(I))

        fig, ax = plt.subplots(3, 2)
        fig.suptitle(self._tag, fontsize=16)
        fig.set_size_inches(12, 9)
        fig.tight_layout()

        totInfections = []
        for k in t:
            totInfections.append(I[k] + E[k] + L[k] + H[k])

        # Day on Day Infections
        DOD = []
        for k in range(len(totInfections) - 1):
            DOD.append(1.0 * totInfections[k + 1] / totInfections[k])

        ax[0, 0].plot(t, totInfections, 'r', label='Infected')
        ax[0, 0].plot(t, I, 'y', label='Infectious')
        ax[0, 0].plot(t, E, 'c', label='Incubating')
        ax[0, 0].plot(t, L, 'm', label='Self-isolating')

        ax[1, 0].plot(t, M, 'b', label='Immune')
        ax[1, 0].plot(t, S, 'm', label='Susceptible')

        ax[0, 1].plot(t, H, 'b', label='In ICU')
        ax[1, 1].plot(t, D, 'k', label='Dead')

        ax[2, 1].plot(range(len(DOD)), DOD, 'r', label='Day-on-day')

        ax[2, 0].plot(range(len(LK)), LK, 'k', label='Lockdown')
        ax[2, 0].plot(range(len(IC)), IC, 'b', label='Infected Clusters')

        # Annotations
        annotations1 = ['{:,.1f}'.format(x/1000) + 'K' for x in totInfections]
        annotations2 = ['{:,.1f}'.format(x/1000) + 'K' for x in M]
        annotations4 = ['{:,.1f}'.format(x/1000) + 'K' for x in S]
        annotations5 = [int(x) for x in D]

        # Annotation steps, minimum 1
        step = max(int(len(I) / 10.0), 1)

        for i, txt in enumerate(annotations1):
            if i % step == 0:
                ax[0, 0].annotate(txt, (t[i], I[i] + E[i] + L[i] + H[i]))

        for i, txt in enumerate(annotations2):
            if i % step == 0:
                ax[1, 0].annotate(txt, (t[i], M[i]))

        for i, txt in enumerate(annotations4):
            if i % step == 0:
                ax[1, 0].annotate(txt, (t[i], S[i]))

        for i, txt in enumerate(annotations5):
            if i % step == 0:
                ax[1, 1].annotate(txt, (t[i], D[i]))

        ax[0, 0].legend()
        ax[1, 0].legend()
        ax[0, 1].legend()
        ax[1, 1].legend()
        ax[2, 1].legend()
        ax[2, 0].legend()

        plt.subplots_adjust(top=0.9)
        plt.savefig('./data_model/SEIR - ' + self._tag + '.png')
        plt.show()