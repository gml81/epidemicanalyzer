import numpy as np
from random import gauss, randint
from .EpiSEIRCluster import EpiSEIRCluster
from .EpiResults import EpiResults

""" 
    ********************************************************************************
    SEIR-COVID Model
    ********************************************************************************
"""
class EpiSEIR:
    
    # Constructor
    # ===========
    def __init__(self, params, tag):

        self.LK = []    # Lockdown
        self.IC = []    # Infected clusters

        self._parseParams(params)
        self._createClusters(params, tag)
        self._results = EpiResults(tag)

    # Parse Params
    # ============
    def _parseParams(self, params):
        self._activateLockdownCases = params['lockdownActiveCases']
        self._liftLockdownCases = params['liftLockdownActiveCases']
        self._lockdown = False
        self._interactions = params['interactions']
        self._interactionsWeights = params['interactionsWeights']
        self._clusterSize = params['E0'] + params['S0'] + params['M0']
        self._maxEpochs = params['maxEpochs']

        EpiSEIRCluster.H_max = params['H_max'] / 100000.0 * self._clusterSize

    # Create clusters
    # ===============
    def _createClusters(self, params, tag):

        # Clusters
        self._clusters = []

        for k in range(params['nbrOfClusters']):

            # Only infect the first cluster
            if k > 0:
                params['E0'] = 0

            self._clusters.append(EpiSEIRCluster(params, tag, k))

    # Run simulations
    # ===============
    def runSimulation(self):

        toBreak = False

        # Run simulation for all epochs
        for epoch in range(self._maxEpochs):

            self._applyLockdown()
            clustersInfected = 0

            # Run simulation for all clusters
            for k in range(len(self._clusters)):
                CC = self._getCrossContamination(k)

                # Check if we need to break
                if self._clusters[k].runSimulation(CC, epoch):
                    toBreak = True

                clustersInfected += self._clusters[k].isInfected()

            self.IC.append(clustersInfected)

            # If error occured or stagnation
            if toBreak or self._stagnation():
                break

        self._printResults()

    # Check if algorithm stagnated
    # ============================
    def _stagnation(self):
        # If no changes are expected
        
        cnt = 0

        for k in range(len(self._clusters)):
            cnt += self._clusters[k].stagnated

        if cnt == len(self._clusters):
            return True
        else:
            return False

    # Get cross contamination
    # =======================
    def _getCrossContamination(self, clusterId):
        
        print('FINE -0135: Calc proj infections cluster[' + str(clusterId) + '] ...')
        proj = 0
        nbrOfClusters = len(self._clusters)
        cA = self._clusters[clusterId]

        for i in range(nbrOfClusters):

            if i == clusterId:
                continue

            # If interaction exists between two clusters
            if self._interactions[clusterId, i] == 1:

                # If lockdown is applicable, probability is that this interaction
                # will cease
                if self._lockdown:
                    test = randint(1, 101)
                    if test > 20:
                        print('FINER-0161:    Cross contam [' + str(clusterId) + ', ' + str(i) + '] = 0')
                        continue

                # Interaction is determined by the following idea:
                # X% of population A is suceptible of getting in contact
                # with population B. Probability of any of these being infected
                # is infectedB/totalB
                cB = self._clusters[i]

                # Calculate interaction, which is the percentage of people
                # from cluster A that are in interaction with B
                interaction = self._interactionsWeights[clusterId, i]

                n = interaction * cA.getSusceptible() * cB.getInfectious() / self._clusterSize
                
                print('FINER-0161:    Cross contam [' + str(clusterId) + ', ' + str(i) + '] = ' + str(n))
                proj += n

        return round(proj)

    # Apply social distancing once the threshold has been turned
    # ==========================================================
    def _applyLockdown(self):

        totInf = 0

        for k in range(len(self._clusters)):
            totInf += self._clusters[k].getInHospital() + self._clusters[k].getInSelfIsolation()

        print('Checking lockdown .... ' + str(totInf))

        # If social distancing is active
        if self._lockdown:
        
            # If the parameter has not been set
            if self._liftLockdownCases <= 0:
                self.LK.append(0)
            
            elif totInf < self._liftLockdownCases:
                self._clustersLiftLockdown()
                self._lockdown = False
                self.LK.append(-5)
                print('INFO - Lifting lockdown')

            else:
                self.LK.append(0)

        else:
            # If the parameter has not been set
            if self._activateLockdownCases <= 0:
                self.LK.append(0)

            elif totInf > self._activateLockdownCases:
                self._clustersApplyLockdown()
                self._lockdown = True
                self.LK.append(5)
                print('INFO - Applying lockdown')

            else:
                self.LK.append(0)

    # Lift lockdown cases
    # ===================
    def _clustersLiftLockdown(self):
        for k in range(len(self._clusters)):
            self._clusters[k].removeSocialDistancing()

    # Apply lockdown cases
    # ====================
    def _clustersApplyLockdown(self):
        for k in range(len(self._clusters)):
            self._clusters[k].applySocialDistancing()

    # Print results
    # =============
    def _printResults(self):
        from operator import add

        S = [] 
        E = []
        I = []
        L = []
        H = []
        M = []
        D = []

        for k in range(len(self._clusters)):
            tS, tE, tI, tL, tH, tM, tD = self._clusters[k].getData()

            if k == 0:
                S.extend(tS)
                E.extend(tE)
                I.extend(tI)
                L.extend(tL)
                H.extend(tH)
                M.extend(tM)
                D.extend(tD)
            
            else:
                S = list(map(add, S, tS))
                E = list(map(add, E, tE))
                I = list(map(add, I, tI))
                L = list(map(add, L, tL))
                H = list(map(add, H, tH))
                M = list(map(add, M, tM))
                D = list(map(add, D, tD))

        # Plot results
        self._results.plotResults2(I, S, E, M, L, H, D, self.LK, self.IC)