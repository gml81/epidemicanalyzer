from math import floor, ceil
import numpy as np

# Step function
# =============
def U(x):
    if x > 0:
        return 1
    else:
        return 0

""" 
    ********************************************************************************
    SEIR-COVID Model
    ********************************************************************************
"""
class EpiSEIRCluster:
    
    H_max = 0

    # Constructor
    # ===========
    def __init__(self, params, tag, idx):

        self._idx = idx
        self._r = params['r']
        self._r0 = params['r']

        self._S0 = params['S0']
        self._E0 = params['E0']
        self._M0 = params['M0']

        self._lockdownActiveCases = params['lockdownActiveCases']
        self._liftLockdownActiveCases = params['liftLockdownActiveCases']
        self._maxEpochs = params['maxEpochs']
        self._growthFunction = params['growthFunction']

        self._populationSize = self._M0 + self._S0 + self._E0

        # Used to stop the simulation under certain conditions
        self._lockdown = False

        # Adjust value of h
        s = params['s']
        d = params['d']
        dh = params['dh']
        h = params['h'] * U(params['tauR'] - params['tauH'])

        # Model hidden params
        self._beta = 1.0 / params['tauE']

        self._gamma = (1 - s) * (1 - h) * (1 - d) / params['tauR']

        self._delta = s / params['tauS']

        self._epsilon = (1 - h) * (1 - d) / (params['tauR'] - params['tauS'])

        self._muI = h * (1 - s) * (1 - d) / params['tauH']
        self._muL = h / (params['tauH'] - params['tauS'])

        self._rhoI = d * (1 - s) * (1 - h) / params['tauD']
        self._rhoL = d * (1 - h) / (params['tauD'] - params['tauS'])

        self._nuH = (1 - dh) / params['tauRH']
        self._rhoH = dh / params['tauDH']

        self.I = []
        self.S = []
        self.E = []
        self.M = []
        self.L = []
        self.H = []
        self.D = []

        self.I.append(self._E0)
        self.S.append(self._populationSize - self._M0 - self._E0)
        self.E.append(0)
        self.L.append(0)
        self.H.append(0)
        self.D.append(0)
        self.M.append(self._M0)

        self.stagnated = False

    # Run simulation
    # ==============
    def runSimulation(self, CC, epoch):

        print ('\n*********** Epoch ' + str(epoch) + " ***********\n")

        deltaS, deltaE, deltaI, deltaM, deltaL, deltaH, deltaD = self._calculateDeltas(CC)
            
        print ('INFO -0065: - M (Immune)         = ' + str(round(self.M[-1], 2)))
        print ('INFO -0050: - S (susceptible)    = ' + str(round(self.S[-1], 2)))
        print ('INFO -0053: - E (Incubating)     = ' + str(round(self.E[-1], 2)))
        print ('INFO -0053: - I (Infected)       = ' + str(round(self.I[-1], 2)))
        print ('INFO -0089: - L (Self-isolation) = ' + str(round(self.L[-1], 2)))
        print ('INFO -0096: - H (Hospital)       = ' + str(round(self.H[-1], 2)))

        # Checkpoints
        return self._performCheckPoint(deltaS, deltaE, deltaI, deltaM, deltaL, deltaH, deltaD)

    # Calculate Deltas
    # ================
    def _calculateDeltas(self, CC):

        # From Susceptible
        rsi = self._getGrowth() + CC
            
        # From Incubating
        bE = self._beta * self.E[-1]

        # From Infected
        aI = self._gamma * self.I[-1]
        sI = self._delta * self.I[-1]

        # Self-isolation
        eL = self._epsilon * self.L[-1]

        # Death
        dI = self._rhoI * self.I[-1]
        dL = self._rhoL * self.L[-1]
        dH = self._rhoH * self.H[-1]

        # Hospitalization
        hI = self._muI * self.I[-1]
        hL = self._muL * self.L[-1]
        aH = self._nuH * self.H[-1]

        hIL = hI + hL
        if hIL > 0:
            deltaHH = self.H[-1] + hIL - self.H_max
            dhIL = deltaHH / hIL * U(deltaHH)

            dhI = dhIL * hI
            dhL = dhIL * hL

            hI = (1 - dhIL) * hI
            hL = (1 - dhIL) * hL

        else:
            dhI = 0
            dhL = 0

        # Save states
        deltaS = -rsi
        deltaE = rsi - bE
        deltaI = bE - aI - sI - hI - dI - dhI
        deltaL = sI - eL - hL - dL - dhL
        deltaH = hI + hL - aH - dH
        deltaM = aI + eL + aH
        deltaD = dI + dH + dL + dhI + dhL

        # Calculate States
        self.S.append(self.S[-1] + deltaS)
        self.I.append(self.I[-1] + deltaI)
        self.M.append(self.M[-1] + deltaM)
        self.E.append(self.E[-1] + deltaE)
        self.L.append(self.L[-1] + deltaL)
        self.H.append(self.H[-1] + deltaH)
        self.D.append(self.D[-1] + deltaD)

        return deltaS, deltaE, deltaI, deltaM, deltaL, deltaH, deltaD

    # Perform checkpoints to find out whether to break
    # ================================================
    def _performCheckPoint(self, deltaS, deltaE, deltaI, deltaM, deltaL, deltaH, deltaD):
        
        # Check if any group is negative
        if round(self.I[-1]) < 0:
            print('ERROR !!! I = ' + str(self.I[-1]))
            return True

        if round(self.E[-1]) < 0:
            print('ERROR !!! E = ' + str(self.E[-1]))
            return True

        if round(self.S[-1]) < 0:
            print('ERROR !!! S = ' + str(self.S[-1]))
            self._printDetails()
            return True

        if round(self.L[-1]) < 0:
            print('ERROR !!! L = ' + str(self.L[-1]))
            self._printDetails()
            return True

        if round(self.H[-1]) < 0:
            print('ERROR !!! H = ' + str(self.H[-1]))
            self._printDetails()
            return True

        if round(self.D[-1]) < 0:
            print('ERROR !!! D = ' + str(self.D[-1]))
            self._printDetails()
            return True

        # Check point on population change
        deltaSum = round(deltaS + deltaE + deltaI + deltaM + deltaL + deltaH + deltaD, 5)
        if not deltaSum == 0:
            print('=====================================')
            print('ERROR !!! Delta = ' + str(deltaSum))

            print(' o DeltaI = ' + str(deltaI))
            print(' o DeltaM = ' + str(deltaM))
            print(' o DeltaS = ' + str(deltaS))
            print(' o DeltaE = ' + str(deltaE))
            print(' o DeltaH = ' + str(deltaH))
            print(' o DeltaD = ' + str(deltaD))

            return True

        # Check point on population size
        deltaSum = round(self.S[-1] + self.E[-1] + self.I[-1] + self.M[-1] + self.L[-1] + self.H[-1] \
            + self.D[-1])

        if not deltaSum == self._populationSize:
            print('ERROR !!! N = ' + str(deltaSum))
            return True

        deltaSum = abs(round(deltaS, 2)) + abs(round(deltaM, 2)) + abs(round(deltaE, 2)) \
            + abs(round(deltaI, 2)) + abs(round(deltaL, 2)) + abs(round(deltaH, 2)) \
                + abs(round(deltaD, 2))

        if deltaSum == 0:
            print('INFO -0243: No more changes on cluster ' + str(self._idx))
            self.stagnated = True
        else:
            self.stagnated = False

        return False

    # Get growth
    # ==========
    def _getGrowth(self):
        
        if self._growthFunction == 'Gompertz':
            r = self._r * np.exp(-4 * np.exp(-4 * self.S[-1] / self._populationSize))
            return r * self.S[-1] * self.I[-1] / self._populationSize

        elif self._growthFunction == 'Linear':
            return self._r * (self.S[-1] ** 2) * self.I[-1] / (self._populationSize ** 2)

        elif self._growthFunction == 'Exp':
            r = self._r * (1 - np.exp(-4 * self.S[-1] / self._populationSize))
            return r * self.S[-1] * self.I[-1] / self._populationSize

        else:
            return self._r * self.S[-1] * self.I[-1] / self._populationSize

    # Print details for debugging purposes
    # ====================================
    def _printDetails(self):

        print('\n')
        print('I = ' + str([round(elem, 2) for elem in self.I]))
        print('S = ' + str([round(elem, 2) for elem in self.S]))
        print('E = ' + str([round(elem, 2) for elem in self.E]))
        print('M = ' + str([round(elem, 2) for elem in self.M]))
        print('L = ' + str([round(elem, 2) for elem in self.L]))
        print('H = ' + str([round(elem, 2) for elem in self.H]))
        print('D = ' + str([round(elem, 2) for elem in self.D]))
        print('\n')

    # Get infectious population
    # =========================
    def getInfectious(self):
        return self.I[-1]

    # Get population in self-isolation
    # ================================
    def getInSelfIsolation(self):
        return self.L[-1]

    # Get susceptible
    # ===============
    def getSusceptible(self):
        return self.S[-1]

    # Get population in hospital
    # ==========================
    def getInHospital(self):
        return self.H[-1]

    # Remove Social Distancing
    # ========================
    def removeSocialDistancing(self):
        self._r = self._r0

    # Apply Social Distancing
    # =======================
    def applySocialDistancing(self):
        self._r = 0.2

    # Return all data
    # ===============
    def getData(self):
        return self.S, self.E, self.I, self.L, self.H, self.M, self.D

    # Is infected
    # ===========
    def isInfected(self):
        return self.S[-1] < self._S0