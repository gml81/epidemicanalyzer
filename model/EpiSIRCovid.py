from .EpiResults import EpiResults
from math import floor, ceil
import numpy as np

# Step function
# =============
def U(x):
    if x > 0:
        return 1
    else:
        return 0

""" 
    ********************************************************************************
    S-I-R Model
    ********************************************************************************
"""
class EpiSIRCovid:
    
    # Constructor
    # ===========
    def __init__(self, params, tag):
        self._r = params['r']
        self._r0 = params['r']
        self._d = params['d']
        self._s = params['s']
        self._h = params['h']
        self._dh = params['dh']

        self._tauE = params['tauE']
        self._tauR = params['tauR']
        self._tauD = params['tauD']
        self._tauS = params['tauS']
        self._tauH = params['tauH']
        self._tauDH = params['tauDH']
        self._tauRH = params['tauRH']

        self._S0 = params['S0']
        self._E0 = params['E0']
        self._M0 = params['M0']

        self._lockdownActiveCases = params['lockdownActiveCases']
        self._liftLockdownActiveCases = params['liftLockdownActiveCases']
        self._maxEpochs = params['maxEpochs']
        self._growthFunction = params['growthFunction']

        self._populationSize = self._M0 + self._S0 + self._E0
        self._H_max = params['H_max'] / 100000.0 * self._populationSize

        self._results = EpiResults(tag)

        # Used to stop the simulation under certain conditions
        self._stopCnt = 0
        self._maxLife = self._tauE + max(self._tauR, self._tauD, self._tauRH, self._tauDH)
        self._lockdown = False

    # Initialize Arrays
    # =================
    def _initArrays(self, I, S, E, M, L, D, H, Z, dhh, LK):

        maxPeriod = max(self._tauR, self._tauD, self._tauH + self._tauDH, \
            self._tauH + self._tauRH)

        self._startEpoch = self._tauE + maxPeriod

        for _ in range (self._startEpoch):
            I.append(0)
            S.append(self._populationSize - self._M0)
            E.append(0)
            M.append(self._M0)
            L.append(0)
            D.append(0)
            H.append(0)
            Z.append(0)
            LK.append(0)
            dhh.append(self._h)

        # Initialize starting values for exposed
        for k in range(1, self._tauE + 1):
            E[-k - 1] = self._E0
            S[-k - 1] = self._populationSize - self._M0 - self._E0

        # I has to be initialized otherwise it wont work
        I[-1] = self._E0
        S[-1] = self._populationSize - self._M0 - self._E0
        E[-1] = 0

        self._startEpoch += 1

        self._printDetails(I, S, E, M, L, D, H, Z, dhh)

    # Run simulation
    # ==============
    def runSimulation(self):

        I = []
        S = []
        E = []
        M = []
        L = []
        D = []
        H = []
        Z = []
        dhh = []
        LK = []

        self._initArrays(I, S, E, M, L, D, H, Z, dhh, LK)

        # Adjust value of h
        self._h = self._h * U(self._tauR - self._tauH)

        # Run cycles
        for i in range(self._startEpoch, self._maxEpochs):

            print ('\n*********** Epoch ' + str(i) + " ***********\n")

            # Apply social distancing
            self._applyLockdown(I, L, H, LK)

            # Calculate deltas
            deltaS, deltaE, deltaI, deltaH, deltaL, deltaM, deltaD = \
                self._calculateDeltas(I, S, E, M, L, D, H, Z, dhh)
            
            print ('INFO -0065: - M (Immune)       = ' + str(round(M[-1], 2)))
            print ('INFO -0046: - D (Dead)         = ' + str(round(D[-1], 2)))
            print ('INFO -0050: - S (susceptible)  = ' + str(round(S[-1], 2)))
            print ('INFO -0053: - E (Incubating)   = ' + str(round(E[-1], 2)))
            print ('INFO -0053: - I (Infected)     = ' + str(round(I[-1], 2)))
            print ('INFO -0048: - L (Isolating)    = ' + str(round(L[-1], 2)))
            print ('INFO -0049: - H (Hospitalized) = ' + str(round(H[-1], 2)))

            # Checkpoints
            toBreak = self._performCheckPoint(deltaS, deltaE, deltaI, deltaH, deltaL, deltaM, deltaD, \
                I, S, E, M, L, D, H, Z, dhh)

            if toBreak:
                break

        # Plot results
        self._results.plotResults(I[20:], S[20:], E[20:], M[20:], L[20:], D[20:], H[20:], LK[20:])

    # Calculate Deltas
    # ================
    def _calculateDeltas(self, I, S, E, M, L, D, H, Z, dhh):

        # From Susceptible
        rsi = self._getGrowth(I, S)
            
        # From Incubating
        bE = Z[-self._tauE]

        # Self-isolation
        sI = self._s * Z[-self._tauE - self._tauS]

        # Hospitalization of Infected
        hI = self._h * (1 - self._s) * Z[-self._tauE - self._tauH]

        # Hospitalization of Self-Isolated
        hL = self._h * self._s * Z[-self._tauE - self._tauH]

        # Death from lack of ICU beds
        hIL = hI + hL

        if hIL > 0:
            deltaHH = H[-1] + hIL - self._H_max
            dhIL = deltaHH / hIL * U(deltaHH)

            dhI = dhIL * hI
            dhL = dhIL * hL

            hI = (1 - dhIL) * hI
            hL = (1 - dhIL) * hL

        else:
            dhIL = 0
            dhI = 0
            dhL = 0

        dI = self._d * (1 - self._s) * (1 - self._h) * Z[-self._tauE - self._tauD]
        dL = self._s * self._d * (1 - self._h) * Z[-self._tauE - self._tauD]

        # Recovery
        aI = (1 - self._s) * (1 - self._d) * (1 - self._h) * Z[-self._tauE - self._tauR]
        aL = self._s * (1 - self._d) * (1 - self._h) * Z[-self._tauE - self._tauR]

        # Death at hospital
        dH = self._dh * dhh[-self._tauDH] * Z[-self._tauE - self._tauH - self._tauDH]
        
        # Recovery at hospital
        aH = (1 - self._dh) * dhh[-self._tauRH] * Z[-self._tauE - self._tauH - self._tauRH]

        # Save states
        deltaS = -rsi
        deltaE = rsi - bE
        deltaI = bE - aI - dI - hI - sI - dhI
        deltaL = sI - dL - aL - hL - dhL
        deltaH = hI + hL - dH - aH
        deltaD = dI + dL + dH + dhI + dhL
        deltaM = aI + aL + aH

        # Calculate States
        S.append(S[-1] + deltaS)
        I.append(I[-1] + deltaI)
        D.append(D[-1] + deltaD)
        H.append(H[-1] + deltaH)
        M.append(M[-1] + deltaM)
        E.append(E[-1] + deltaE)
        L.append(L[-1] + deltaL)
        Z.append(rsi)
        dhh.append(self._h * (1 - dhIL))

        return deltaS, deltaE, deltaI, deltaH, deltaL, deltaM, deltaD

    # Perform checkpoints to find out whether to break
    # ================================================
    def _performCheckPoint(self, deltaS, deltaE, deltaI, deltaH, deltaL, deltaM, deltaD, \
        I, S, E, M, L, D, H, Z, dhh):
        
        # Check point on population change
        deltaSum = round(deltaS + deltaD + deltaE + deltaI + deltaH + deltaM + deltaL, 5)
        if not deltaSum == 0:
            print('=====================================')
            print('ERROR !!! Delta = ' + str(deltaSum))

            print(' o DeltaI = ' + str(deltaI))
            print(' o DeltaM = ' + str(deltaM))
            print(' o DeltaS = ' + str(deltaS))
            print(' o DeltaE = ' + str(deltaE))
            print(' o DeltaL = ' + str(deltaL))
            print(' o DeltaH = ' + str(deltaH))
            print(' o DeltaD = ' + str(deltaD))

            return True

        # Check if any group is negative
        if round(I[-1]) < 0:
            print('ERROR !!! I = ' + str(I[-1]))
            return True

        if round(H[-1]) < 0:
            print('ERROR !!! H = ' + str(H[-1]))
            self._printDetails(I, S, E, M, L, D, H, Z, dhh)
            return True

        if round(E[-1]) < 0:
            print('ERROR !!! E = ' + str(E[-1]))
            return True

        if round(S[-1]) < 0:
            print('ERROR !!! S = ' + str(S[-1]))
            self._printDetails(I, S, E, M, L, D, H, Z, dhh)
            return True

        if round(L[-1]) < 0:
            print('ERROR !!! L = ' + str(L[-1]))
            return True

        # Check point on population size
        deltaSum = round(S[-1] + D[-1] + E[-1] + I[-1] + H[-1] + M[-1] + L[-1])
        if not deltaSum == self._populationSize:
            print('ERROR !!! N = ' + str(deltaSum))
            return True
        
        # If no changes are expected
        deltaSum = abs(round(deltaS)) + abs(round(deltaH)) + abs(round(deltaD)) \
            + abs(round(deltaM)) + abs(round(deltaL)) + abs(round(deltaE)) + abs(round(deltaI))

        if deltaSum == 0:
            self._stopCnt += 1

            if round(L[-1] + E[-1] + I[-1] + H[-1]) == self._E0:
                print('INFO -0243: No more changes, Stopping!')
                return True

            if self._stopCnt > self._maxLife:
                print('INFO -0248: Algorithm stagnated, Stopping!')
                return True

        else:
            self._stopCnt = 0

        return False

    # Get growth
    # ==========
    def _getGrowth(self, I, S):
        
        if self._growthFunction == 'Gompertz':
            r = self._r * np.exp(-4 * np.exp(-4 * S[-1] / self._populationSize))
            return r * S[-1] * I[-1] / self._populationSize

        elif self._growthFunction == 'Linear':
            return self._r * (S[-1] ** 2) * I[-1] / (self._populationSize ** 2)

        elif self._growthFunction == 'Exp':
            r = self._r * (1 - np.exp(-4 * S[-1] / self._populationSize))
            return r * S[-1] * I[-1] / self._populationSize

        else:
            return self._r * S[-1] * I[-1] / self._populationSize

    # Apply social distancing once the threshold has been turned
    # ==========================================================
    def _applyLockdown(self, I, L, H, LK):

        # If social distancing is active
        if self._lockdown:
        
            # If the parameter has not been set
            if self._liftLockdownActiveCases <= 0:
                LK.append(0)
            
            elif I[-1] + L[-1] + H[-1] < self._liftLockdownActiveCases:
                self._r = self._r0
                self._lockdown = False
                LK.append(-20)
                print('INFO - Lifting lockdown, r = ' + str(self._r))

            else:
                LK.append(0)

        else:
            # If the parameter has not been set
            if self._lockdownActiveCases <= 0:
                LK.append(0)

            elif I[-1] + L[-1] + H[-1] > self._lockdownActiveCases:
                self._r = 0.2
                self._lockdown = True
                LK.append(20)
                print('INFO - Applying lockdown, r = ' + str(self._r))

            else:
                LK.append(0)

    # Print details for debugging purposes
    # ====================================
    def _printDetails(self, I, S, E, M, L, D, H, Z, dhh):
        print('\n')
        print('dhh = ' + str([round(elem, 2) for elem in dhh[-26:]]))
        print('Z = ' + str([round(elem) for elem in Z[-26:]]))
        print('I = ' + str([round(elem, 2) for elem in I[-26:]]))
        print('S = ' + str([round(elem, 2) for elem in S[-26:]]))
        print('E = ' + str([round(elem, 2) for elem in E[-26:]]))
        print('M = ' + str([round(elem, 2) for elem in M[-26:]]))
        print('L = ' + str([round(elem, 2) for elem in L[-26:]]))
        print('D = ' + str([round(elem, 2) for elem in D[-26:]]))
        print('\n')