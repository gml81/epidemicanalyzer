from random import lognormvariate
import matplotlib.pyplot as plt
import numpy as np

# Plot age distribution
# =====================
def plotAgeDistribution(ageDist):
    fig1, (ax2) = plt.subplots(1)
    fig1.suptitle("Age Distribution", fontsize=16)
    fig1.set_size_inches(7, 4)

    counts, bins = np.histogram(ageDist, bins=100)
    ax2.hist(bins[:-1], bins, weights=counts)

    plt.show()

#####################################################################################
# Generate Age Distribution
#####################################################################################
def generateAgeDistribution(averageAge):
    ageDist = []

    for i in range(2000):
        ageDist.append(int(averageAge * (lognormvariate(0, 0.5) - 0.5)))
        
        while ageDist[i] <= 0 or ageDist[i] > 100:
            ageDist[i] = int(averageAge * (lognormvariate(0, 0.5) - 0.5))

    plotAgeDistribution(ageDist)

    return ageDist