from sim.EpiParticle import EpiParticle
import random
from random import gauss

# Generate cluster size, gaussian distribution
# ============================================
def generateClusterSize(p):
    a = int(gauss(p, p / 2))

    if a < 5:
        return generateClusterSize(p)
    else:
        return a

""" 
    ********************************************************************************
    Epidemic Cluster
    ********************************************************************************
"""
class EpiCluster:
    
    # Constructor
    # ===========
    def __init__(self, idx, params):
        self._idx = idx
        self._hasbeenInfected = False
        self._clusterSize = generateClusterSize(params['populationSize'])

        # Init variables
        self._clusterImmune = False

        print('INFO -0034: Created cluster ' + str(idx) + ' ...')

        # Create all particles
        self._particles = []
        for i in range(self._clusterSize):
            self._particles.append(EpiParticle(i, params))

    # Cluster size
    # ============
    def clusterSize(self):
        return self._clusterSize

    # Infect particle
    # ===============
    def infectParticle(self, j):
        self._particles[j].infectParticle()

    # Update epoch
    # ============
    def update(self, projectedInfections):

        proj = int(projectedInfections)

        # Get total of particles in final states
        tot = self._currentlyDead + self._currentlyImmune
        sTot = tot + self._currentlySusceptible

        # If nobody is infected or recovering, update is not required
        if tot == self._clusterSize or (proj == 0 and sTot == self._clusterSize):
            print('Passing ...')

        # Update is required
        else:
            for i in range(proj):
                # Get a random particle in the cluster to infect
                idxToInfect = random.randint(0, self._clusterSize - 1)
                self._particles[idxToInfect].infectParticle()

            # Update all particles
            for i in range(self._clusterSize):
                self._particles[i].update()

            self.generateStats()

        # If all population is immune
        if self._currentlyImmune == self._clusterSize:
            self._clusterImmune = True

        # If has been infected once
        if not self._hasbeenInfected and self._currentlyIncubating > 0:
            self._hasbeenInfected = True

    # Generate stats
    # ==============
    def generateStats(self):
        # Get stats
        self._currentlyInfected = 0
        self._currentlyInfectious = 0
        self._currentlyImmune = 0
        self._currentlyDead = 0
        self._currentlyIsolating = 0
        self._currentlyIncubating = 0
        self._currentlyInHospital = 0
        self._currentlyLostYears = 0
        self._currentlySusceptible = 0

        # Calculate statistics
        for i in range(self._clusterSize):

            p = self._particles[i]
            
            self._currentlyIsolating += p.isSelfIsolating()
            self._currentlyInfected += p.isInfected()
            self._currentlyInfectious += p.isInfectious()
            self._currentlyImmune += p.isImmune()
            self._currentlyDead += p.isDead()
            self._currentlyIncubating += p.isIncubating()
            self._currentlyInHospital += p.isInHospital()
            self._currentlyLostYears += p.lostYears()
            self._currentlySusceptible += p.isSusceptible()

        """
        print('FINE -0112: Cluster [' + str(self._idx) + '] stats:')
        print('FINE -0184:     Incubating  = ' + str(self._currentlyIncubating))
        print('FINE -0113:     Infected    = ' + str(self._currentlyInfected))
        print('FINE -0113:     Infectious  = ' + str(self._currentlyInfectious))
        print('FINE -0114:     Immune      = ' + str(self._currentlyImmune))
        print('FINE -0115:     Isolating   = ' + str(self._currentlyIsolating))
        print('FINE -0116:     Dead        = ' + str(self._currentlyDead))
        print('FINE -0116:     In hospital = ' + str(self._currentlyInHospital))
        print('FINE -0125:     Susceptible = ' + str(self._currentlySusceptible))
        print('FINE -0125:     Lost years  = ' + str(self._currentlyLostYears))
        """

    # Get number of particles currently infected
    # ===========================================
    def getInfected(self):
        return self._currentlyInfected

    # Get number of lost years
    # ========================
    def getLostYears(self):
        return self._currentlyLostYears

    # Get number of particles currently in hospital
    # =============================================
    def getInHospital(self):
        return self._currentlyInHospital

    # Get self isolated
    # =================
    def getSelfIsolated(self):
        return self._currentlyIsolating

    # Get incubating
    # ==============
    def getIncubating(self):
        return self._currentlyIncubating

    # Get flag on has been infected
    # =============================
    def hasBeenInfected(self):
        return self._hasbeenInfected

    # Is cluster infected
    # ===================
    def infected(self):
        for i in range(self._clusterSize):

            if self._particles[i].isInfected() or self._particles[i].isSelfIsolating() \
                    or self._particles[i].isIncubating() \
                        or self._particles[i].isInHospital():
                return True
        
        return False

    # Get infected
    # ============
    def getImmune(self):
        return self._currentlyImmune

    # Get particle number who died in this cycle
    # ==========================================
    def getDead(self):
        return self._currentlyDead

    # Get currently infectious population
    # ===================================
    def getInfectious(self):
        return self._currentlyInfectious

    # Has cluster attained immunity
    # =============================
    def isImmune(self):
        return self._clusterImmune

    # Append age data
    # ================
    def appendAge(self):
        data = []

        for i in range(self._clusterSize):
            
            p = self._particles[i]
            data.append(p.getAge())

        return data
        
    # Get suceptible population
    # =========================
    def getSusceptible(self):
        return self._currentlySusceptible
