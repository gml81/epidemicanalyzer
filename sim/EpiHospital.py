""" 
    ********************************************************************************
    Epidemic Hospital Simulation
    ********************************************************************************
"""
class EpiHospital:

    maxCapacity = 0
    currentCount = 0

    # Admit patient to hospital
    # =========================
    def admit():
        if EpiHospital.currentCount < EpiHospital.maxCapacity:
            EpiHospital.currentCount += 1
            return True
        else:
            return False

    # Check out
    #==========
    def checkOut():
        EpiHospital.currentCount -= 1