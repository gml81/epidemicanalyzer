from random import randint, gauss, lognormvariate
import random as rnd
import math
import numpy as np
from .EpiHospital import EpiHospital

""" 
    ********************************************************************************
    Epidemic Particle
    ********************************************************************************
"""
class EpiParticle:

    # Constructor
    def __init__(self, idx, params):
        self._idx = idx
        self._calculateAge(params)
        self._yearsLost = 0

        # Initialize states
        self._infectedState = _InfectedState(params, self._age)
        self._healthyState = _HealthyState()
        self._immuneState = _ImmuneState()
        self._deathState = _DeathState()
        self._selfIsolationState = _SelfIsolationState()
        self._incubationState = _IncubationState(params)
        self._hospitalizationState = _Hospitalizationtate()

        self._startingStateBasedOnAge(params)

    # Update status of particle in system
    # ===================================
    def _startingStateBasedOnAge(self, params):
        # Set current state, depends on age group and whether or not children are immune
        if params['childrenImmune']:
            if self._age < 10:
                self._currentState = self._immuneState
            elif self._age < 20:
                # If between 20 and 30, 50% chance of being immune
                test = test = randint(1, 101)
                if test < 50:
                    self._currentState = self._immuneState
                else:
                    self._currentState = self._healthyState
            # If above 30, particle is not immune
            else:
                self._currentState = self._healthyState
        else:
            self._currentState = self._healthyState

    # Update status of particle in system
    # ===================================
    def update(self):
        self._currentState.update(self)

    # Is immune
    # =========
    def isImmune(self):
        return isinstance(self._currentState, _ImmuneState)

    # Self Isolating
    # ==============
    def isSelfIsolating(self):
        return isinstance(self._currentState, _SelfIsolationState)

    # Hospitalization
    # ===============
    def isInHospital(self):
        return isinstance(self._currentState, _Hospitalizationtate)

    # Self Infected
    # ==============
    def isInfected(self):
        return isinstance(self._currentState, _InfectedState) 

    # Is infectious
    # =============
    def isInfectious(self):
        return self._currentState.isInfectious()

    # Is susceptible
    # ==============
    def isSusceptible(self):
        return self._currentState.isSusceptible()

    # Is dead
    # =======
    def isDead(self):
        return isinstance(self._currentState, _DeathState)

    # Lost years
    # ==========
    def lostYears(self):
        return self._yearsLost

    # Infect Particle
    # ===============
    def infectParticle(self):
        self._currentState.infect(self)

    # Is in incubation period
    # =======================
    def isIncubating(self):
        return isinstance(self._currentState, _IncubationState)

    # Start virus incubation state
    # ============================
    def startIncubation(self):
        self._currentState = self._incubationState

    # Change state to immune
    # ======================
    def immunize(self):
        self._currentState = self._immuneState

    # Change state to infected
    # ========================
    def onsetInfection(self):
        self._currentState = self._infectedState

    # Change state to dead
    # ====================
    def die(self):
        self._currentState = self._deathState
        self._yearsLost = max(0, 80 - self.getAge())

    # Self isolation
    # ==============
    def selfIsolate(self, daysInfected, daysToRecover, daysToICUAdmission):
        self._selfIsolationState.updateDaysInfected(daysInfected, daysToRecover, \
            daysToICUAdmission)
        self._currentState = self._selfIsolationState

    # Hospitalize
    # ===========
    def hospitalize(self, daysInfected, daysToRecover):
        self._hospitalizationState.updateDaysInfected(daysInfected, daysToRecover)
        self._currentState = self._hospitalizationState

    # Calculate Age
    # =============
    def _calculateAge(self, params):
        self._age = np.random.choice(params['ageDist'])

    # Get age
    # =======
    def getAge(self):
        return self._age

""" 
    ********************************************************************************
    Incubation period
    ********************************************************************************
"""
class _IncubationState:

    # Constructor
    # ===========
    def __init__(self, params):
        self._incubationPeriod = int(params['incubationPeriod'] * lognormvariate(0, 0.4))
        self._daysInfected = 0

    # Update state
    # ============    
    def update(self, particle):
        self._daysInfected += 1

        # If we are still in incubation period
        if self._daysInfected < self._incubationPeriod:
            pass
        else:
            particle.onsetInfection()

    # Infect
    # ======
    def infect(self, particle):
        pass

    # Is infectious
    # =============
    def isInfectious(self):
        return False

    # Is susceptible
    # ==============
    def isSusceptible(self):
        return False

""" 
    ********************************************************************************
    Infected state
    ********************************************************************************
"""
class _InfectedState:

    # Constructor
    # ===========
    def __init__(self, params, particleAge):
        self._daysToICUAdmission = int(params['daysToICUAdmission'] * lognormvariate(0, 0.25))
        self._selfIsolationRequired = params['selfIsolation']

        self._daysInfected = 0

        # There is a 20% chance that infected particle will not self-isolate
        test = randint(1, 101)
        if test < 20:
            self._openToSelfIsolate = False
        else:
            self._openToSelfIsolate = True

        # Is the particle asymptomatic
        if particleAge < 40:
            test = randint(1, 101)

            if test < params['percAsymptomatic']:
                self._isAsymptomatic = True
            else:
                self._isAsymptomatic = False
        else:
            self._isAsymptomatic = False

        # If the particle is asymptomatic, it will not be subject to treatment
        rp = int(params['daysToRecover'] * lognormvariate(0, 0.5))

        if self._isAsymptomatic:
            self._daysToRecover = rp
        else:
            if params['daysToRecoverWithTreatment'] > 0:
                self._daysToRecover = round(params['daysToRecoverWithTreatment'] * lognormvariate(0, 0.5))
            else:
                self._daysToRecover = rp
        
        self._symptomOnset = 0.2 * self._daysToRecover

    # Update state
    # ============    
    def update(self, particle):
        self._daysInfected += 1

        # If particle has recovered
        if self._daysInfected >= self._daysToRecover:
            particle.immunize()
            return

        if self._isAsymptomatic:
            return
        else:
            # If particle has developed symptoms
            if self._daysToRecover > self._symptomOnset:
                if self._openToSelfIsolate:
                    if self._selfIsolationRequired:
                        particle.selfIsolate(self._daysInfected, self._daysToRecover, self._daysToICUAdmission)
                        return

            # Check if requires hospitalization
            if self._daysInfected >= self._daysToICUAdmission:
                if _requiresHospitalization(particle.getAge()):
                    if EpiHospital.admit():
                        particle.hospitalize(self._daysInfected, self._daysToRecover)
                    else:
                        particle.die()

    # Infect, do nothing as already infected
    # ======================================
    def infect(self, particle):
        pass

    # Is infectious
    # =============
    def isInfectious(self):
        return _getInfectionsness(self._daysInfected, self._daysToRecover)

    # Is susceptible
    # ==============
    def isSusceptible(self):
        return False

""" 
    ********************************************************************************
    Healthy state
    ********************************************************************************
"""
class _HealthyState:

    # Update state
    # ============    
    def update(self, particle):
       pass
    
    # Infect
    # ======
    def infect(self, particle):
        particle.startIncubation()

    # Is infectious
    # =============
    def isInfectious(self):
        return False

    # Is susceptible
    # ==============
    def isSusceptible(self):
        return True

""" 
    ********************************************************************************
    Self Isolation State
    ********************************************************************************
"""
class _SelfIsolationState:

    # Constructor
    # ===========
    def __init__(self):
        pass

    # Update days infected before using this state
    # ============================================
    def updateDaysInfected(self, daysInfected, daysToRecover, daysToICUAdmission):
        self._daysInfected = daysInfected
        self._daysToRecover = daysToRecover
        self._daysToICUAdmission = daysToICUAdmission

    # Update state
    # ============    
    def update(self, particle):
        self._daysInfected += 1

        # If particle has not yet recovered
        if self._daysInfected >= self._daysToRecover:
            particle.immunize()
            return

        # Check if requires hospitalization
        if self._daysInfected >= self._daysToICUAdmission:
            if _requiresHospitalization(particle.getAge()):
                if EpiHospital.admit():
                    particle.hospitalize(self._daysInfected, self._daysToRecover)
                else:
                    particle.die()
    
    # Infect
    # ======
    def infect(self, particle):
        pass

    # Is infectious
    # =============
    def isInfectious(self):
        return False

    # Is susceptible
    # ==============
    def isSusceptible(self):
        return False

""" 
    ********************************************************************************
    Immune state
    ********************************************************************************
"""
class _ImmuneState:

    # Update state
    # ============    
    def update(self, particle):
       pass

     # Infect, do nothing as already infected
    # ======================================
    def infect(self, particle):
        pass

    # Is infectious
    # =============
    def isInfectious(self):
        return False

    # Is susceptible
    # ==============
    def isSusceptible(self):
        return False

""" 
    ********************************************************************************
    Death state
    ********************************************************************************
"""
class _DeathState:

    # Update state
    # ============    
    def update(self, particle):
       pass

     # Infect, do nothing as already infected
    # ======================================
    def infect(self, particle):
        pass

    # Is infectious
    # =============
    def isInfectious(self):
        return False

    # Is susceptible
    # ==============
    def isSusceptible(self):
        return False

""" 
    ********************************************************************************
    Hosipitalization state
    ********************************************************************************
"""
class _Hospitalizationtate:

    # Constructor
    # ===========
    def __init__(self):
        pass

    # Update days infected before using this state
    # ============================================
    def updateDaysInfected(self, daysInfected, daysToRecover):
        self._daysInfected = daysInfected
        self._daysToRecover = daysToRecover

    # Update state
    # ============    
    def update(self, particle):
        self._daysInfected += 1

        # If particle has not yet recovered
        if self._daysInfected >= self._daysToRecover:
            particle.immunize()
            EpiHospital.checkOut()

        # Check if particle dies after spending time in ICU
        elif self._daysInfected >= 0.75 * self._daysToRecover:
            test = randint(1, 101)
            if test > 60:
                particle.die()
                EpiHospital.checkOut()
    
    # Infect
    # ======
    def infect(self, particle):
        pass

    # Is infectious
    # =============
    def isInfectious(self):
        return False

    # Is susceptible
    # ==============
    def isSusceptible(self):
        return False

#####################################################################################
# Common functions
#####################################################################################

# Requires Hospitalization
# ========================
def _requiresHospitalization(particleAge):

    test = randint(1, 101)

    # Age = 80
    if particleAge > 80:
        if test < 30:
            return True

    # Age = 70
    elif particleAge > 70:
        if test < 16:
            return True

    # Age = 60
    elif particleAge > 60:
        if test > 8:
            return True

    # Age = 50
    elif particleAge > 50:
        if test > 4:
            return True

    return False
    
# Infectiousness
# ==============
def  _getInfectionsness(daysInfected, daysToRecover):
    if daysInfected > daysToRecover:
        return 0

    return 1 - (1.0 * daysInfected / daysToRecover)