import numpy as np
import matplotlib.pyplot as plt
import csv
from .EpiHospital import EpiHospital

""" 
    ********************************************************************************
    Epidemic Results
    ********************************************************************************
"""

class EpiResults:

     # Constructor
    # ===========
    def __init__(self, tag):
        # Data points to track infections and immune
        self._infectionData = []
        self._infectionClustersData = []
        self._infectiousData = []
        self._immuneData = []
        self._deadData = []
        self._selfIsolatedData = []
        self._susceptibleData = []
        self._lockdown = []
        self._lostYearsData = []
        self._incubationData = []
        self._hospitalizedData = []
        self._dayOnDayInfections = []

        # Parameters
        self._tag = tag
        self._firstEntry = True

    # Update stats
    # ============
    def updateCounters(self, newInfections, newInfectious, newImmune, newLostYears, \
        newSelfIsolated, newSusceptible, infectedClusters, newDead, newIncubation, newHospital):

        # Substract the number of initially immune
        """
        if self._firstEntry:
            self._firstEntry = False
            self._initiallyImmune = newImmune

        # Remove number of people initially immune otherwise it will skew the graphs
        newImmune -= self._initiallyImmune

        print ('INFO -0065: - Immune population      = ' + str(newImmune))
        #print ('INFO -0045: - Init Immune population = ' + str(self._initiallyImmune))
        print ('INFO -0046: - Dead population        = ' + str(newDead))
        print ('INFO -0047: - Infected population    = ' + str(newInfections))
        print ('INFO -0048: - Infectious population  = ' + str(newInfectious))
        print ('INFO -0048: - Incubation             = ' + str(newIncubation))
        print ('INFO -0049: - Self Isolating         = ' + str(newSelfIsolated))
        print ('INFO -0050: - Susceptible            = ' + str(newSusceptible))
        print ('INFO -0051: - Infected Clusters      = ' + str(infectedClusters))
        print ('INFO -0054: - In Hospital            = ' + str(EpiHospital.currentCount))
        """
        
        if len(self._infectionData) == 0 or self._infectionData[-1] == 0:
            self._dayOnDayInfections.append(0)
        else:
            self._dayOnDayInfections.append(1.0 * newInfections / self._infectionData[-1])

        self._infectiousData.append(newInfectious)
        self._immuneData.append(newImmune)
        self._deadData.append(newDead)
        self._selfIsolatedData.append(newSelfIsolated)
        self._infectionData.append(newInfections)
        self._susceptibleData.append(newSusceptible)
        self._infectionClustersData.append(infectedClusters)
        self._lostYearsData.append(newLostYears)
        self._incubationData.append(newIncubation)
        self._hospitalizedData.append(newHospital)

    # Register lockdown
    # =================
    def registerLockdown(self, tag):
        self._lockdown.append(tag)

    # Dump data to file
    # =================
    def _dumpResultsToFile(self, tag):
        with open('./data/csv_file_' + tag + '.csv', mode='w', newline='') as employee_file:
            
            employee_writer = csv.writer(employee_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            employee_writer.writerow(['Infections', 'Infectious', 'Immune', 'Dead', 
                'SelfIsolated', 'Susceptible', 'Clusters', 'In Hospital'])

            for k in range(len(self._infectionData)):
                employee_writer.writerow([self._infectionData[k], 
                    self._infectiousData[k], self._immuneData[k], self._deadData[k], 
                    self._selfIsolatedData[k], self._susceptibleData[k], 
                    self._infectionClustersData[k], self._hospitalizedData[k]])

    # Plot results
    # ============
    def plotResults(self, tag):
        
        # self._dumpResultsToFile(tag)

        t = range(len(self._infectionData))

        fig, ax = plt.subplots(3, 2)
        fig.suptitle(tag, fontsize=16)
        fig.set_size_inches(12, 9)
        fig.tight_layout()

        ax[0, 0].plot(t, self._infectionData, 'r', label='Infected')
        ax[0, 0].plot(t, self._infectiousData, 'y', label='Infectious')
        ax[0, 0].plot(t, self._selfIsolatedData, 'g', label='Self Isolating')
        ax[0, 0].plot(t, self._incubationData, 'c', label='Incubating')

        ax[0, 1].plot(t, self._immuneData, 'b', label='Immune')
        ax[0, 1].plot(t, self._susceptibleData, 'm', label='Susceptible')

        ax[1, 0].plot(t, self._infectionClustersData, 'c', label='Infected Clusters')

        ax[1, 1].plot(t, self._deadData, 'r', label='Cumulative Death')
        ax[1, 1].plot(t, self._lostYearsData, 'y', label='Cumulative Lost Years')

        ax[2, 0].plot(t, self._hospitalizedData, 'b', label='Hospitalized')

        ax[2, 1].plot(t, self._dayOnDayInfections, 'r', label='Day on Day Infections')

        if len(self._lockdown) == len(self._infectionData):
            ax[2, 0].plot(t, self._lockdown, 'k', label='Lockdown')

        # Annotations
        annotations1 = ['{:,.1f}'.format(x/1000) + 'K' for x in self._infectionData]
        annotations2 = ['{:,.1f}'.format(x/1000) + 'K' for x in self._immuneData]
        annotations3 = list(map(str, self._infectionClustersData))
        annotations4 = ['{:,.1f}'.format(x/1000) + 'K' for x in self._susceptibleData]
        annotations5 = [int(x) for x in self._deadData]
        annotations6 = [int(x) for x in self._lostYearsData]

        # Annotation steps, minimum 1
        step = max(int(len(self._infectionData) / 5.0), 1)

        for i, txt in enumerate(annotations1):
            if i % step == 0:
                ax[0, 0].annotate(txt, (t[i], self._infectionData[i]))

        for i, txt in enumerate(annotations2):
            if i % step == 0:
                ax[0, 1].annotate(txt, (t[i], self._immuneData[i]))

        for i, txt in enumerate(annotations4):
            if i % step == 0:
                ax[0, 1].annotate(txt, (t[i], self._susceptibleData[i]))

        for i, txt in enumerate(annotations3):
            if i % step == 0:
                ax[1, 0].annotate(txt, (t[i], self._infectionClustersData[i]))

        for i, txt in enumerate(annotations5):
            if i % step == 0:
                ax[1, 1].annotate(txt, (t[i], self._deadData[i]))

        for i, txt in enumerate(annotations6):
            if i % step == 0:
                ax[1, 1].annotate(txt, (t[i], self._lostYearsData[i]))

        ax[0, 0].legend()
        ax[0, 1].legend()
        ax[1, 0].legend()
        ax[1, 1].legend()
        ax[2, 1].legend()

        plt.subplots_adjust(top=0.9)
        plt.savefig('./data/SIM - ' + self._tag + '.png')
        plt.show()
