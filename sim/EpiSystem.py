import array
from random import gauss, randint
import numpy as np
from .EpiResults import EpiResults
from .EpiParticle import EpiParticle
from .EpiCluster import EpiCluster
from .EpiHospital import EpiHospital

""" 
    ********************************************************************************
    Epidemic Simulator
    ********************************************************************************
"""
class EpiSystem:

    # Constructor
    # ===========
    def __init__(self, params, tag):
        self._clusters = []
        self._params = params
        self._tag = tag
        self._nbrOfClusters = params['numberOfClusters']
        self._lockdownActive = params['lockdownActive']
        self._activateLockdownCases = params['activateLockdownCases']
        self._liftLockdownCases = params['liftLockdownCases']
        self._contactRate = params['contactRate']
        self._lockdown = False
        self._selfIsolations = 0
        self._interactions = params['interactions']
        self._interactionsWeights = params['interactionsWeights']
        self._populationSize = 0

        # Calculate social distance
        self._socialDistance = self._switchSocialDistanceType(params['clusterSocialDistanceType'])
        self._interclusterSocialDistance = self._switchSocialDistanceType(params['interClusterSocialDistanceType'])

        # Results
        self._results = EpiResults(tag)

        # Clusters
        self._createClusters(params['numberOfClusters'])

        # Update ICU beds per 100K peopel
        EpiHospital.maxCapacity = params['MaxHospitalCapacity'] * self._populationSize / 100000

    # Run simulation
    # ==============
    def runSimulation(self):

        # Run epochs
        for i in range(self._params['epochs']):
            print ('INFO -0069: =====================================================')
            print ('INFO -0045: Running epoch ' + str(i))

            if self._simulateSystemEvolution():
                self._updateLockDown()
                break

            if self._lockdownActive:
                self._updateLockDown()

        self._results.plotResults(self._tag)

    # Simulate epidemic spread in system
    # ==================================
    def _simulateSystemEvolution(self):
        
        # Stats and counters
        currentlyInfected = 0
        self._selfIsolations = 0
        currentlyImmune = 0
        currentlyDead = 0
        immuneClusters = 0
        infectedClusters = 0
        currentlyInfectious = 0
        currentlyIncubating = 0
        hasBeenInfected = 0
        currentlyInHospital = 0
        currentlyLostYears = 0

        # Simulate cycle
        for i in range(self._nbrOfClusters):
            
            c = self._clusters[i]
            if c.isImmune():
                immuneClusters += 1
            else:
                # Get the projected infections for this cycle
                projInfections = self._getProjectedInfections(i)
                
                """
                print ('INFO -0078: SYS->Projected infections cluster [' + str(i) + ']  = ' 
                    + str(projInfections))
                """

                # Update clusers
                c.update(projInfections)

                # Update data
                self._selfIsolations += c.getSelfIsolated()
                currentlyInfected += c.getInfected()
                currentlyInfectious += c.getInfectious()
                currentlyIncubating += c.getIncubating()
                currentlyInHospital += c.getInHospital()

            currentlyLostYears += c.getLostYears()
            currentlyImmune += c.getImmune()
            currentlyDead += c.getDead()

            # Keep track of number of infected clusters
            if c.infected():
                infectedClusters += 1
        
            # Keep track of number of total infected clusters
            if c.hasBeenInfected():
                hasBeenInfected += 1

        # Total infections is infected + self-isolating + incubating + in hospital
        currentlyInfected += self._selfIsolations + currentlyIncubating \
            + currentlyInHospital
        
        # Number of susceptible
        currentlySusceptible = self._populationSize - currentlyInfected \
            - currentlyImmune - currentlyDead

        currentlyInfectious = int(currentlyInfectious)

        self._results.updateCounters(currentlyInfected, currentlyInfectious, currentlyImmune, 
            currentlyLostYears, self._selfIsolations, currentlySusceptible, hasBeenInfected,
            currentlyDead, currentlyIncubating, currentlyInHospital)

        # If system is immune, break
        if immuneClusters == self._nbrOfClusters:
            print('INFO -0076: All clusters immune')
            return True

        if currentlyInfected == 0 or infectedClusters == 0:
            print('INFO -0138: No infected clusters')
            return True

        # Verify totals
        tot = currentlyDead + currentlyImmune + currentlyInfected + currentlySusceptible
        if not tot == self._populationSize:
            print('ERROR: tot = ' + str(tot) + ' != ' + str(self._populationSize))
            return True

        return False
        
    # Create clusters
    # ===============
    def _createClusters(self, nbrClusters):
        print('INFO -0022: Creating clusters ...')

        for i in range(nbrClusters):
            self._clusters.append(EpiCluster(i, self._params))

            self._populationSize += self._clusters[i].clusterSize()

        # Start 10 infections in at least one cluster
        for i in range(min(nbrClusters, 4)):
            for j in range(5):
                self._clusters[i].infectParticle(j)
            
        for i in range(nbrClusters):
            self._clusters[i].generateStats()

    # Calculate the number of projected infections in this cluster
    # ============================================================
    def _getProjectedInfections(self, clusterId):
        print('FINE -0135: Calc proj infections cluster[' + str(clusterId) + '] ...')
        proj = 0

        for i in range(self._nbrOfClusters):
            # If interaction exists between two clusters
            if self._interactions[clusterId, i] == 1:

                # If lockdown is applicable, probability is that this interaction
                # will cease
                if self._lockdown:
                    test = randint(1, 101)
                    if test > 20:
                        continue

                # If its the cluster in question, add all infected
                if i == clusterId:
                    n = self._clusters[i].getInfectious() * self._contactRate * \
                        self._interclusterSocialDistance
                else:
                    # Interaction is determined by the following idea:
                    # X% of population A is suceptible of getting in contact
                    # with population B. Probability of any of these being infected
                    # is infectedB/totalB
                    cB = self._clusters[i]
                    riskCrossContam = cB.getInfectious() / cB.clusterSize()

                    size = self._clusters[clusterId].clusterSize()

                    # Calculate interaction, which is the percentage of people
                    # from cluster A that are in interaction with B
                    interaction = self._interactionsWeights[clusterId, i]

                    n = (interaction * size) * (riskCrossContam * self._socialDistance)
                
                print('FINER-0161:    Cross contam [' + str(clusterId) + ', ' + str(i) + '] = ' + str(n))
                proj += n

        return round(proj)

    # Control lockdown
    # ================
    def _updateLockDown(self):
        # Lockdown tag
        tag = 0

        # If lockdown is in process
        if self._lockdown:
            if self._selfIsolations < self._liftLockdownCases:
                self._lockdown = False
                
                print('****************************************************')
                print('Lifting lockdown ...')
                
                tag = -4

        else:
            if self._selfIsolations > self._activateLockdownCases:
                self._lockdown = True
                
                print('****************************************************')
                print('Initiating lockdown ...')

                tag = 4
        
        self._results.registerLockdown(tag)

    # Get social distance
    # ===================
    def _switchSocialDistanceType(self, argument):
        switcher = {
            'High' : 0.1,
            'Medium': 0.4,
            'Low': 1
        }
        return switcher.get(argument, 1)