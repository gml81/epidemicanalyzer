from sim.Params import ModelParams
from sim.EpiSystem import EpiSystem

""" 
    ********************************************************************************
    Epidemic Simulator
    ********************************************************************************
"""
class EpideSim:

    # Constructor
    # ===========
    def __init__(self, params, tag):
        mParams = ModelParams(params)

        _params = mParams.params

        if not _params is None:
            self._sim = EpiSystem(_params, tag)
        else:
            print('ERROR-0020: Error in params, aborting!')

    # Run simulation
    # ==============
    def runSimulation(self):
        self._sim.runSimulation()

    # Get results
    # ===========
    def getResults(self):
        return self._sim.resultAnalysis()
