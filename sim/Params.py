""" 
    ********************************************************************************
    Epidemic Simulator - Model Parameters
    ********************************************************************************
"""
class ModelParams:

    # Constructor
    # ===========
    def __init__(self, params):
        if self.validateParams(params):
            self.params = params
        else:
            self.params = None

    # Validate input parameters
    # =========================
    def validateParams(self, params):
        print ('INFO -0004: Validating parms ...')

        # Population size greater than 0
        if (params['populationSize'] <= 0):
            print ('ERROR-0007: Parameter populationSize lte 0')
            return False

        # Population size less than 1000000
        if (params['populationSize'] > 1000000):
            print ('ERROR-0007: Parameter populationSize > 1000000')
            return False

        # Number of clusters
        if not 'numberOfClusters' in params:
            print ('ERROR-0028: Parameter numberOfClusters not defined')
            return False
        else:
            if (params['numberOfClusters'] <= 0):
                print ('ERROR-0027: Parameter numberOfClusters lte 0')
                return False

        # Contact Rate
        if not 'contactRate' in params:
            print ('ERROR-0044: Parameter contactRate not defined')
            return False
        else:
            if params['contactRate'] <= 0:
                print ('ERROR-0048: Parameter contactRate lte 0')
                return False

        # Self Isolation
        if not 'selfIsolation' in params:
            print ('ERROR-0053: Parameter selfIsolation not defined')
            return False
        else:
            if not params['selfIsolation'] in [True, False]:
                print ('ERROR-0057: Parameter selfIsolation not in [True, False]')
                return False

        # Lockdown Active
        if not 'lockdownActive' in params:
            print ('ERROR-0062: Parameter lockdownActive not defined')
            return False
        else:
            if not params['lockdownActive'] in [True, False]:
                print ('ERROR-0157: Parameter lockdownActive not in [True, False]')
                return False

        # Activate Lockdown Cases
        if not 'activateLockdownCases' in params:
            print ('ERROR-0181: Parameter activateLockdownCases not defined')
            return False
        else:
            if params['activateLockdownCases'] < 0:
                print ('ERROR-0185: Parameter activateLockdownCases invalid')
                return False

        # Activate Lockdown Cases
        if not 'liftLockdownCases' in params:
            print ('ERROR-0281: Parameter liftLockdownCases not defined')
            return False
        else:
            if params['liftLockdownCases'] < -1:
                print ('ERROR-0285: Parameter liftLockdownCases invalid')
                return False

        # Immunity in children
        if not 'childrenImmune' in params:
            print ('ERROR-0289: Parameter childrenImmune not defined')
            return False
        else:
            if not params['childrenImmune'] in [True, False]:
                print ('ERROR-0193: Parameter childrenImmune not in [True, False]')
                return False

        # Days To Recover with Treatment
        if not 'daysToRecoverWithTreatment' in params:
            print ('ERROR-0253: Parameter daysToRecoverWithTreatment not defined')
            return False

        # Days To Recover
        if not 'daysToRecover' in params:
            print ('ERROR-0053: Parameter daysToRecover not defined')
            return False
        else:
            if params['daysToRecover'] <= 0:
                print ('ERROR-0057: Parameter daysToRecover lte 0')
                return False

        # Days To ICU Admission
        if not 'daysToICUAdmission' in params:
            print ('ERROR-0105: Parameter daysToICUAdmission not defined')
            return False
        else:
            if params['daysToICUAdmission'] <= 0:
                print ('ERROR-0057: Parameter daysToICUAdmission lte 0')
                return False

        # Maximum hospital capacity
        if not 'MaxHospitalCapacity' in params:
            print ('ERROR-0105: Parameter MaxHospitalCapacity not defined')
            return False
        else:
            if (params['MaxHospitalCapacity'] <= 0):
                print ('ERROR-0109: Parameter MaxHospitalCapacity lte 0')
                return False

        # Percentage of Aysmptomatic
        if not 'percAsymptomatic' in params:
            print ('ERROR-0090: Parameter percAsymptomatic not defined')
            return False
        else:
            if (params['percAsymptomatic'] < 0 or params['percAsymptomatic'] > 100):
                print ('ERROR-0194: Parameter percAsymptomatic invalid')
                return False

        # Incubation Period
        if not 'incubationPeriod' in params:
            print ('ERROR-0191: Parameter incubationPeriod not defined')
            return False
        else:
            if (params['incubationPeriod'] < 0 or params['incubationPeriod'] > 100):
                print ('ERROR-0094: Parameter incubationPeriod invalid')
                return False

        # Social Distance Type
        if not 'clusterSocialDistanceType' in params:
            print ('ERROR-0051: Parameter clusterSocialDistanceType not defined')
            return False
        else:
            types = ['High' , 'Medium', 'Low']

            if not params['clusterSocialDistanceType'] in types:
                print ('ERROR-0057: Parameter clusterSocialDistanceType invalid')
                return False
            
        # Inter-cluster Social Distance Type
        if not 'interClusterSocialDistanceType' in params:
            print ('ERROR-0151: Parameter interClusterSocialDistanceType not defined')
            return False
        else:
            types = ['High' , 'Medium', 'Low']

            if not params['interClusterSocialDistanceType'] in types:
                print ('ERROR-0157: Parameter interClusterSocialDistanceType invalid')
                return False
            
        return True
